/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	__webpack_require__(1);

	__webpack_require__(3);

	__webpack_require__(5);

	__webpack_require__(6);

	__webpack_require__(4);

	__webpack_require__(2);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _boardloader = __webpack_require__(2);

	console.log(_boardloader.loadBoards);

	function authorize() {
	    console.log('got here');
	    Trello.authorize({
	        type: "popup",
	        name: "Trello dashboard",
	        scope: {
	            read: true,
	            write: true
	        },
	        expiration: "never",
	        success: _boardloader.loadBoards,
	        error: function error() {
	            console.log("Failed authentication");
	        }
	    });
	}

	exports.default = authorize();

/***/ },
/* 2 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.loadBoards = loadBoards;
	/**
	 * Created by DEV on 6/3/2016.
	 */
	var loadedBoards = function loadedBoards(boards) {
	    $.each(boards, function (index, value) {
	        $('#boards').append($("<option></option>").attr("value", value.id).text(value.name));
	    });
	};

	function loadBoards() {
	    Trello.get('/members/me/boards/', loadedBoards, function () {
	        console.log("Failed to load boards");
	    });
	}

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _allboard = __webpack_require__(4);

	$('#addnewcardsendbtn').click(function (e) {
	    e.preventDefault();
	    var ListId = document.getElementById('stash5').textContent;
	    var newCard = {
	        name: document.getElementById('cardtexthl').value,
	        pos: "top",
	        idList: ListId
	    };

	    Trello.post('/cards/', newCard, function () {
	        console.log("Sucess"); // add event
	    }, function () {
	        console.log("Failed");
	    });

	    (0, _allboard.activeAllDataAfterAddCard)();
	    $('#cardsshowall').hide(500).show(1000);
	    $('#filterdarea').hide(200);
	});

	$('#addcardslist').click(function () {
	    $('#stash5').text($('#addcardslist').val());
	});

	var loadedList = function loadedList(Lists) {
	    $.each(Lists, function (index, value) {
	        $('#addcardslist').append($("<option></option>").attr("value", value.id).text(value.name));
	    });
	};

	$('#all').click(function () {
	    var boardId = document.getElementById('boards')[1].value;
	    Trello.get('/boards/' + boardId + '/lists/', loadedList, function () {
	        console.log("Failed to load List");
	    });
	});

	////var loadMemebers = (members, cardId, cardName, cardElment)=> {
	////    let missionString = " mission: ";
	////    let memberString = ' member: ';
	////
	////    cardElment.empty();
	////    cardElment.append('<p>'+missionString+ cardName +memberString+members.name+ '</p>');
	////};
	//
	//var loadedMembers = (members)=>{
	//     $.each(members, (index, value)=> {
	//        $('#memberslist')
	//            .append($("<option></option>")
	//                .attr("value", value.id)
	//                .text(value.fullName));
	//    });
	//};
	//
	//

	//
	//$('#alllist').click(()=> {
	//    var boardId = document.getElementById('boards')[1].value;
	//    Trello.get(
	//        '/boards/' + boardId + '/members/',
	//        loadedMembers,
	//        function () {
	//            console.log("Failed to load Members");
	//        }
	//    );
	//});
	//
	//
	//
	//function cardsDetailsOnMouseOver(elementid)
	//{
	//    let idString  =  String(elementid);
	//    $('#'+idString + ' > p').show(500);
	//}
	//
	//function cardsDetailsOnMouseOut(elementid)
	//{
	//    let idString  =  String(elementid);
	//    $('#'+idString + ' > p').hide(500);
	//}
	//
	//var loadCards = function(Cards) {
	//    $('#cardarea').empty();
	//    $('#showcardsoflist').show(500);
	//    $.each(Cards, function(index, value) {
	//
	//        let closed = '';
	//        let due = '';
	//        if(value.due == null)
	//        {
	//            due = "There no end date";
	//        }
	//        else
	//        {
	//            due = value.due;
	//        }
	//
	//        if(value.closed  == true)
	//        {
	//            closed = "Closed"
	//        }
	//        else
	//        {
	//            closed = "Open"
	//        }
	//
	//        let substring = 'subCardId'+index;
	//        $('#cardarea').addClass('allcard').append('<ul>' + '<h1 ' + 'id='+'cardId'+index+  ' value=' + value.id +
	//            ' onmouseover='+ "function(substring){$('#'+substring).show(100);}"+
	//           'onmouseout='+  "function(substring){$('#'+substring).hide(100);}"+ '>'
	//            + value.name + '</h1>' +
	//            '<p'+ ' id=' +substring +' style=' + 'display:'+ 'none'+'>' +
	//            " Status: " +  closed  +" End Date: "+ due +'</p>' +'</ul>');
	//
	//    });
	//    $('#showcardsoflist').show(1000);
	//    $('html, body').animate({scrollTop:400}, 'slow');
	//
	//};
	//

	//
	//
	//
	//$('#outlist').change(function() {
	//    let ListId = $("option:selected", this).val();
	//    $('#stash2').text(ListId);
	//    let action = document.getElementById('stash').textContent;
	//    let url = '/lists/' + ListId + '/cards/';
	//    if (action == "2") {
	//        $('#showcardsoflist').hide(500);
	//        $('#addcardtolist').show(1000);
	//        $('html, body').animate({scrollTop:500}, 'slow');
	//    }
	//    else if (action == "1") {
	//        Trello.get(
	//        url,
	//        loadCards,
	//        ()=> {
	//            console.log("Failed to load cards");
	//        }
	//    );
	//
	//    }
	//
	//
	//
	//});
	//
	//
	//
	//$('#memberslist').change(function () {
	//    let MemberId = $("option:selected", this).val();
	//    let url = 'members/' + MemberId + '/cards/';
	//
	//    Trello.get(
	//        url,
	//        loadCards,
	//        ()=> {
	//            console.log("Failed to load cards");
	//        }
	//    );
	//});
	//
	//
	//
	//$('#filterwith').change(function () {
	//    if($("option:selected", this).val() == "1"){
	//        $('#members').show(1000);
	//        $('#list').hide(500);
	//         $('html, body').animate({scrollTop:500}, 'slow');
	//
	//    }
	//
	//    if($("option:selected", this).val() == "2"){
	//     $('#list').show(1000);
	//     $('#members').hide(500);
	//      $('html, body').animate({scrollTop:500}, 'slow');
	//    }
	//
	//    if($("option:selected", this).val() == "3"){
	//        $('html, body').animate({scrollTop:500}, 'slow');
	//    }
	//
	//});
	//
	//
	//
	//
	//
	//
	//$('#outcardschoose').change(function () {
	//    $('#stash').text($("option:selected", this).val());
	//    if($("option:selected", this).val() == "1"){
	//     $('#filter').show(1000);
	//        $('html, body').animate({scrollTop:500}, 'slow');
	//        $('#addcardtolist').hide(500);
	//         $('#list').hide(500);
	//
	//    }
	//    else{
	//         $('#list').show(1000);
	//        $('#filter').hide(500);
	//        $('#cardarea').hide(500);
	//        $('#members').hide(500);
	//        $('html, body').animate({scrollTop:500}, 'slow');
	//    }
	//
	//});
	//
	//
	//function splitCardtoData() {
	//    console.log("po");
	//    //let cardName = cardElment.text;
	//    //let cardId = cardElment.value;
	//    //let url = 'cards/' + cardId + '/members';
	//    //Trello.get(url, (members)=> {
	//    //    loadMemebers(members, cardId, cardName,cardElment)
	//    //}, ()=> {
	//    //    console.log("Failed to load cards");
	//    //});
	//}

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.activeAllDataAfterAddCard = activeAllDataAfterAddCard;

	var _filterMembers = __webpack_require__(5);

	var getfilterdcardfromlistID = function getfilterdcardfromlistID(cards, cardArea, listarea) {
	    $('#cardsshowall').hide(200);
	    $('#filterdarea').show(1000);
	    var text = $('#textfilterbox').val();
	    var ChoosenCard = void 0;
	    var userchhose = $('#stash4').text();

	    switch (userchhose) {
	        case "hl":
	            ChoosenCard = cards.filter(function (cards) {
	                return cards.name === text;
	            });
	            break;
	        case "date":
	            ChoosenCard = cards.filter(function (cards) {
	                return cards.due.indexOf(text);
	            });
	            break;
	    }

	    if (ChoosenCard.length != 0) {
	        $.each(ChoosenCard, function (index, value) {
	            var finishColor = void 0;
	            var lastDate = void 0;
	            if (value.closed == true) {
	                finishColor = "green";
	            } else {
	                finishColor = "red";
	            }
	            if (value.due != null) {
	                var stringDate = String(value.due);
	                lastDate = stringDate.split('T')[0];
	            } else {
	                lastDate = "There is no end date";
	            }

	            cardArea.append('<div' + ' id=' + value.id + ' ><p>' + value.name + '</p></div>').css('color', finishColor);
	            $('#' + value.id).append('<div class="modal-content" style="display: none" >' + '<div class="modal-header">' + '<span class="close">&times</span>' + '<h2>' + value.name + '</h2>' + '</div>' + '<div class="modal-body">' + '<p>' + 'Status: ' + value.closed + '</p>' + '<p>' + 'Last Date: ' + lastDate + '</p>' + '</div>' + '<div class="modal-footer">' + '</div>' + '</div>').hover(function () {
	                var childModal = $('#' + value.id).children('div');
	                console.log(childModal);
	                childModal.show();
	            }).mouseout(function () {
	                var childModal = $('#' + value.id).children('div');
	                console.log(childModal);
	                childModal.hide();
	            });
	        });
	        cardArea.height(function (index, height) {
	            return height + 155;
	        });
	        listarea.position({
	            of: $(window)
	        });
	    } else {
	        listarea.empty();
	    }

	    $('#filterdarea').show(2000);
	};

	var getFilteredCards = function getFilteredCards(listId, cardArea, listarea) {
	    Trello.get('/lists/' + listId + '/cards/', function (cards) {
	        getfilterdcardfromlistID(cards, cardArea, listarea);
	    }, function () {
	        console.log("Failed");
	    });
	};

	var loadedListwithfilter = function loadedListwithfilter(Lists) {
	    ////
	    $('#filterdarea').empty();
	    $('#filterdarea').hide(1000);

	    var part0 = '<div + id=filterdList';
	    var part1 = ' class="cards"  onClick="top.focus()" style="position: absolute; left:';
	    var left_after_part1 = 150;
	    var generealIndex = 0;
	    var part2 = 'px">';
	    var part3 = '<div class="w3-card-4" style="width:25%;height: 25px">';
	    var part4 = '<header class="w3-container w3-blue" style="height: 75px">';
	    var part5 = '<h1 style="font-size: large; font-family:Harrington">';
	    var part52 = '</h1>' + '</header>';
	    var part6 = '<div class="w3-container" style="height: 89px; font-family:Harrington"' + 'id=filterdcard';
	    var part66 = '>' + '</div>';
	    var part7 = '<footer class="w3-container w3-blue" style="height: 20px"> </footer> </div>';

	    $.each(Lists, function (index, value) {

	        $('#filterdarea').append(part0 + index + part1 + left_after_part1 + part2 + part3 + part4 + part5 + value.name + part52 + part6 + index + part66 + part7);
	        left_after_part1 = left_after_part1 + 350;
	        generealIndex = index;
	        //$('#card' + index).attr("value", value.id);
	        getFilteredCards(value.id, $('#filterdcard' + index), $('#filterdList' + index));
	    });
	};

	//let BuildCardsByFilterd = (cards)=>{
	//    $('#cardsshowall').hide();
	//    $('#filterdarea').show(1000);
	//
	//
	//
	//};

	//
	//let loadedFilterdCards = (Cards)=> {
	//    let userchhose =  $('#stash4').text();
	//    let text = $('#textfilterbox').val();
	//    let ChoosenCard;
	//    if(userchhose == "hl")
	//    {
	//          ChoosenCard = Cards.filter((card)=>{
	//        return card.name === text
	//     });
	//
	//      BuildCardsByFilterd(ChoosenCard);
	//    }
	//
	//    else if(userchhose == "members")
	//    {
	//          ChoosenCard = Cards.filter((card)=>{
	//        return card.name === text
	//     });
	//
	//      BuildCardsByFilterd(ChoosenCard);
	//    }
	//
	//    else
	//    {
	//          ChoosenCard = Cards.filter((card)=>{
	//        return card.name === text
	//     });
	//
	//      BuildCardsByFilterd(ChoosenCard);
	//    }
	//
	//
	//
	//
	//
	//};

	$("#filtersendbtn").click(function (e) {
	    e.preventDefault();
	    var choose = $('#stash4').text();
	    getAllLists(choose);
	});

	$('#filterChoosen').click(function () {
	    $('#stash4').text($('#filterChoosen').val());
	    if ($('#filterChoosen').val() == 'date') {
	        $('#textfilterbox').attr("placeholder", "dd/mm/yy");
	    }
	    if ($('#filterChoosen').val() == 'members') {
	        $('#textfilterbox').attr("placeholder", "filter by friend name..");
	    }
	    if ($('#filterChoosen').val() == 'hl') {
	        $('#textfilterbox').attr("placeholder", "filter by card head line..");
	    }
	});

	var loadedCards = function loadedCards(Cards, thisCardArea) {
	    $.each(Cards, function (index, value) {
	        var finishColor = void 0;
	        var lastDate = void 0;

	        if (value.closed == true) {
	            finishColor = "green";
	        } else {
	            finishColor = "red";
	        }

	        if (value.due != null) {
	            var stringDate = String(value.due);
	            lastDate = stringDate.split('T')[0];
	        } else {
	            lastDate = "There is no end date";
	        }

	        thisCardArea.append('<div' + ' id=' + value.id + ' ><p>' + value.name + '</p></div>').css('color', finishColor);
	        $('#' + value.id).append('<div class="modal-content" style="display: none" >' + '<div class="modal-header">' + '<span class="close">&times</span>' + '<h2>' + value.name + '</h2>' + '</div>' + '<div class="modal-body">' + '<p>' + 'Status: ' + value.closed + '</p>' + '<p>' + 'Last Date: ' + lastDate + '</p>' + '</div>' + '<div class="modal-footer">' + '</div>' + '</div>').hover(function () {
	            var childModal = $('#' + value.id).children('div');
	            console.log(childModal);
	            childModal.show();
	        }).mouseout(function () {
	            var childModal = $('#' + value.id).children('div');
	            console.log(childModal);
	            childModal.hide();
	        });
	    });

	    thisCardArea.height(function (index, height) {
	        return height + 155;
	    });
	};

	var getCards = function getCards(listId, cardArea) {

	    Trello.get('/lists/' + listId + '/cards/', function (cards) {
	        loadedCards(cards, cardArea);
	    }, function () {
	        console.log("Failed to load labels");
	    });
	};

	var loadedList = function loadedList(Lists) {

	    var part1 = '<div class="cards flex-item"  onClick="top.focus()" style="position: absolute; left:';
	    var left_after_part1 = 150;
	    var generealIndex = 0;
	    var part2 = 'px">';
	    var part3 = '<div class="w3-card-4" style="width:25%;height: 25px">';
	    var part4 = '<header class="w3-container w3-blue" style="height: 75px">';
	    var part5 = '<h1 style="font-size: large; font-family:Harrington">';
	    var part52 = '</h1>' + '</header>';
	    var part6 = '<div class="w3-container" style="height: 89px; font-family:Harrington"' + 'id=card';
	    var part66 = '>' + '</div>';
	    var part7 = '<footer class="w3-container w3-blue" style="height: 20px"> </footer> </div>';
	    $.each(Lists, function (index, value) {
	        $('#cardsshowall').append(part1 + left_after_part1 + part2 + part3 + part4 + part5 + value.name + part52 + part6 + index + part66 + part7);
	        left_after_part1 = left_after_part1 + 350;
	        generealIndex = index;
	        //$('#card' + index).attr("value", value.id);
	        getCards(value.id, $('#card' + index));
	    });
	};

	function activeAllDataAfterAddCard() {
	    getAllLists("nofilter");
	}

	var getAllLists = function getAllLists(activatedFrom) {

	    $('#cardsshowall').empty();
	    $('#filter2').show(1000);
	    var boardId = document.getElementById('boards')[1].value;

	    if (activatedFrom == "nofilter") {
	        Trello.get('/boards/' + boardId + '/lists/', loadedList, function () {
	            console.log("Failed to load labels");
	        });
	    } else if (activatedFrom == "members") {
	        Trello.get('/boards/' + boardId + '/members/', _filterMembers.GetAllCards, function () {
	            console.log("Failed");
	        });
	    } else {
	        Trello.get('/boards/' + boardId + '/lists/', loadedListwithfilter, function () {
	            console.log("Failed to load labels");
	        });
	    }
	};

	$('#all').click(function () {
	    getAllLists("nofilter");
	});

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.GetAllCards = GetAllCards;
	var ShowCards = function ShowCards(cardArea, listarea, Card, index) {

	    $('#cardsshowall').hide(200);
	    $('#filterdarea').show(1000);

	    var finishColor = void 0;
	    var lastDate = void 0;
	    if (Card.closed == true) {
	        finishColor = "green";
	    } else {
	        finishColor = "red";
	    }
	    if (Card.due != null) {
	        var stringDate = String(Card.due);
	        lastDate = stringDate.split('T')[0];
	    } else {
	        lastDate = "There is no end date";
	    }

	    cardArea.append('<div' + ' id=' + Card.id + ' ><p>' + Card.name + '</p></div>').css('color', finishColor);
	    $('#' + Card.id).append('<div class="modal-content" style="display: none" >' + '<div class="modal-header">' + '<span class="close">&times</span>' + '<h2>' + Card.name + '</h2>' + '</div>' + '<div class="modal-body">' + '<p>' + 'Status: ' + Card.closed + '</p>' + '<p>' + 'Last Date: ' + lastDate + '</p>' + '</div>' + '<div class="modal-footer">' + '</div>' + '</div>').hover(function () {
	        var childModal = $('#' + Card.id).children('div');
	        console.log(childModal);
	        childModal.show();
	    }).mouseout(function () {
	        var childModal = $('#' + Card.id).children('div');
	        console.log(childModal);
	        childModal.hide();
	    });

	    cardArea.height(function (index, height) {
	        return height + 155;
	    });
	    listarea.position({
	        of: $(window)
	    });

	    $('#filterdarea').show(2000);
	};

	var loadedListwithfilter = function loadedListwithfilter(List, Card, leftElemnetMove, index) {
	    ////
	    console.log('index' + index);
	    console.log('leftElemnetMove' + leftElemnetMove);
	    console.log('List' + List);
	    console.log('Card' + Card);

	    $('#filterdarea').empty();
	    $('#filterdarea').hide(1000);

	    var part0 = '<div + id=filterdList';
	    var part1 = ' class="cards"  onClick="top.focus()" style="position: absolute; left:';
	    var left_after_part1 = leftElemnetMove;
	    var part2 = 'px">';
	    var part3 = '<div class="w3-card-4" style="width:25%;height: 25px">';
	    var part4 = '<header class="w3-container w3-blue" style="height: 75px">';
	    var part5 = '<h1 style="font-size: large; font-family:Harrington">';
	    var part52 = '</h1>' + '</header>';
	    var part6 = '<div class="w3-container" style="height: 89px; font-family:Harrington"' + 'id=filterdcard';
	    var part66 = '>' + '</div>';
	    var part7 = '<footer class="w3-container w3-blue" style="height: 20px"> </footer> </div>';

	    $('#filterdarea').append(part0 + index + part1 + left_after_part1 + part2 + part3 + part4 + part5 + List.name + part52 + part6 + index + part66 + part7);
	    ShowCards($('#filterdcard' + index), $('#filterdList' + index), Card, index);
	};

	function GetAllCards(members) {

	    var boardId = document.getElementById('boards')[1].value;
	    Trello.get('/boards/' + boardId + '/cards/', function (cards) {
	        GetMembersByResponsibleMembers(members, cards);
	    }, function () {
	        console.log("Failed");
	    });
	}

	var GetMembersByResponsibleMembers = function GetMembersByResponsibleMembers(allMembers, allCards) {
	    var MemberFullName = $('#textfilterbox').val();
	    var ChoosenCardsList = [];
	    var ChoosenCards = void 0;
	    ChoosenCards = allCards.filter(function (cards) {
	        var index = void 0;
	        var membersIndex = void 0;
	        var CardMembersOwnersIdList = cards.idMembers;
	        for (index = 0; index < CardMembersOwnersIdList.length; index++) {
	            for (membersIndex = 0; membersIndex < allMembers.length; membersIndex++) {

	                if (allMembers[membersIndex].id == CardMembersOwnersIdList[index] && allMembers[membersIndex].fullName == MemberFullName) {
	                    return cards;
	                }
	            }
	        }
	    });
	    console.log(ChoosenCards);
	    GetAllListByCards(ChoosenCards);
	};

	var GetAllListByCards = function GetAllListByCards(FilterdCards) {
	    var leftElemnetMove = 150;
	    $.each(FilterdCards, function (index, card) {

	        Trello.get('/cards/' + card.id + '/list/', function (lists) {
	            loadedListwithfilter(lists, card, leftElemnetMove, index);
	        }, function () {
	            console.log("Failed");
	        });
	        leftElemnetMove = leftElemnetMove + 350;
	    });
	};

/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';

	$("#alltasks").click(function () {
	    $('#trello-boards').show(1000, function () {
	        $('#trello-cards').hide(500);
	        $('#trello-lists').hide(500);
	    });
	});

	$("#allboards").click(function () {
	    $('#trello-cards').show(1000, function () {
	        $('#trello-all').hide(500);
	        $('#trello-lists').hide(500);
	    });
	});

	$("#alllist").click(function () {
	    $('#trello-lists').show(1000, function () {
	        $('#trello-all').hide(500);
	        $('#trello-cards').hide(500);
	    });
	});

	$("#all").click(function () {
	    $('#trello-all').show(1000, function () {
	        $('#trello-lists').hide(500);
	        $('#trello-cards').hide(500);
	    });
	});

/***/ }
/******/ ]);