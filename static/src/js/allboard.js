import {GetAllCards} from "./filterMembers";


var getfilterdcardfromlistID = (cards, cardArea, listarea)=> {
    $('#cardsshowall').hide(200);
    $('#filterdarea').show(1000);
    let text = $('#textfilterbox').val();
    let ChoosenCard;
    let userchhose = $('#stash4').text();

    switch (userchhose) {
        case "hl":
            ChoosenCard = cards.filter((cards)=> {
                return cards.name === text
            });
            break;
        case "date":
            ChoosenCard = cards.filter((cards)=> {
                return cards.due.indexOf(text);
            });
            break;
    }


    if (ChoosenCard.length != 0) {
        $.each(ChoosenCard, (index, value)=> {
            let finishColor;
            let lastDate;
            if (value.closed == true) {
                finishColor = "green";
            }
            else {
                finishColor = "red";
            }
            if(value.due != null)
            {
                let stringDate = String(value.due);
                lastDate = stringDate.split('T')[0]
            }
            else
            {
                lastDate = "There is no end date"
            }

            cardArea.append('<div' + ' id=' + value.id + ' ><p>' + value.name + '</p></div>').css('color', finishColor);
            $('#' + value.id).append(
                    '<div class="modal-content" style="display: none" >' +
                    '<div class="modal-header">' +
                    '<span class="close">&times</span>' +
                    '<h2>' + value.name + '</h2>' + '</div>' + '<div class="modal-body">'
                    + '<p>' + 'Status: ' + value.closed + '</p>' +
                    '<p>' + 'Last Date: ' + lastDate + '</p>' +
                    '</div>' +
                    '<div class="modal-footer">' + '</div>' + '</div>'
                )

                .hover(
                    ()=> {
                        let childModal = $('#' + value.id).children('div');
                        console.log(childModal);
                        childModal.show();
                    }
                ).mouseout( ()=> {
                let childModal = $('#' + value.id).children('div');
                        console.log(childModal);
                        childModal.hide();
            });




        });
        cardArea.height(function (index, height) {
            return (height + 155);
        });
        listarea.position({
            of: $(window)
        });
    }
    else {
        listarea.empty();

    }

    $('#filterdarea').show(2000);
};

let getFilteredCards = (listId, cardArea, listarea)=> {
    Trello.get(
        '/lists/' + listId + '/cards/',
        (cards)=> {
            getfilterdcardfromlistID(cards, cardArea, listarea)
        },
        function () {
            console.log("Failed");
        }
    );
};


var loadedListwithfilter = (Lists)=> {////
    $('#filterdarea').empty();
    $('#filterdarea').hide(1000);

    let part0 = '<div + id=filterdList';
    let part1 = ' class="cards"  onClick="top.focus()" style="position: absolute; left:';
    let left_after_part1 = 150;
    let generealIndex = 0;
    let part2 = 'px">';
    let part3 = '<div class="w3-card-4" style="width:25%;height: 25px">';
    let part4 = '<header class="w3-container w3-blue" style="height: 75px">';
    let part5 = '<h1 style="font-size: large; font-family:Harrington">';
    let part52 = '</h1>' + '</header>';
    let part6 = '<div class="w3-container" style="height: 89px; font-family:Harrington"' + 'id=filterdcard';
    let part66 = '>' + '</div>';
    let part7 = '<footer class="w3-container w3-blue" style="height: 20px"> </footer> </div>';


    $.each(Lists, (index, value)=> {

        $('#filterdarea')

            .append(part0 + index + part1 + left_after_part1 + part2 + part3 + part4 + part5 + value.name + part52 + part6 + index + part66 + part7);
        left_after_part1 = left_after_part1 + 350;
        generealIndex = index;
        //$('#card' + index).attr("value", value.id);
        getFilteredCards(value.id, $('#filterdcard' + index), $('#filterdList' + index));

    });

};


//let BuildCardsByFilterd = (cards)=>{
//    $('#cardsshowall').hide();
//    $('#filterdarea').show(1000);
//
//
//
//};


//
//let loadedFilterdCards = (Cards)=> {
//    let userchhose =  $('#stash4').text();
//    let text = $('#textfilterbox').val();
//    let ChoosenCard;
//    if(userchhose == "hl")
//    {
//          ChoosenCard = Cards.filter((card)=>{
//        return card.name === text
//     });
//
//      BuildCardsByFilterd(ChoosenCard);
//    }
//
//    else if(userchhose == "members")
//    {
//          ChoosenCard = Cards.filter((card)=>{
//        return card.name === text
//     });
//
//      BuildCardsByFilterd(ChoosenCard);
//    }
//
//    else
//    {
//          ChoosenCard = Cards.filter((card)=>{
//        return card.name === text
//     });
//
//      BuildCardsByFilterd(ChoosenCard);
//    }
//
//
//
//
//
//};


$("#filtersendbtn").click((e)=> {
    e.preventDefault();
    let choose = $('#stash4').text();
    getAllLists(choose);

});


$('#filterChoosen').click(()=> {
    $('#stash4').text($('#filterChoosen').val());
    if ($('#filterChoosen').val() == 'date') {
        $('#textfilterbox').attr("placeholder", "dd/mm/yy");
    }
    if ($('#filterChoosen').val() == 'members') {
        $('#textfilterbox').attr("placeholder", "filter by friend name..");
    }
    if ($('#filterChoosen').val() == 'hl') {
        $('#textfilterbox').attr("placeholder", "filter by card head line..");
    }

});


var loadedCards = (Cards, thisCardArea)=> {
    $.each(Cards, (index, value)=> {
        let finishColor;
        let lastDate;

        if (value.closed == true) {
            finishColor = "green";
        }
        else {
            finishColor = "red";
        }



        if (value.due != null) {
            let stringDate = String(value.due);
            lastDate = stringDate.split('T')[0]
        }
        else {
            lastDate = "There is no end date"
        }


         thisCardArea.append('<div' + ' id=' + value.id + ' ><p>' + value.name + '</p></div>').css('color', finishColor);
            $('#' + value.id).append(
                    '<div class="modal-content" style="display: none" >' +
                    '<div class="modal-header">' +
                    '<span class="close">&times</span>' +
                    '<h2>' + value.name + '</h2>' + '</div>' + '<div class="modal-body">'
                    + '<p>' + 'Status: ' + value.closed + '</p>' +
                    '<p>' + 'Last Date: ' + lastDate + '</p>' +
                    '</div>' +
                    '<div class="modal-footer">' + '</div>' + '</div>'
                )

                .hover(
                    ()=> {
                        let childModal = $('#' + value.id).children('div');
                        console.log(childModal);
                        childModal.show();
                    }
                ).mouseout( ()=> {
                let childModal = $('#' + value.id).children('div');
                        console.log(childModal);
                        childModal.hide();
            });

    });



    thisCardArea.height(function (index, height) {
        return (height + 155);
    });
};


var getCards = (listId, cardArea)=> {

    Trello.get(
        '/lists/' + listId + '/cards/',
        (cards)=> {
            loadedCards(cards, cardArea)
        },
        function () {
            console.log("Failed to load labels");
        }
    );
};



var loadedList = (Lists)=> {

    let part1 = '<div class="cards flex-item"  onClick="top.focus()" style="position: absolute; left:';
    let left_after_part1 = 150;
    let generealIndex = 0;
    let part2 = 'px">';
    let part3 = '<div class="w3-card-4" style="width:25%;height: 25px">';
    let part4 = '<header class="w3-container w3-blue" style="height: 75px">';
    let part5 = '<h1 style="font-size: large; font-family:Harrington">';
    let part52 = '</h1>' + '</header>';
    let part6 = '<div class="w3-container" style="height: 89px; font-family:Harrington"' + 'id=card';
    let part66 = '>' + '</div>';
    let part7 = '<footer class="w3-container w3-blue" style="height: 20px"> </footer> </div>';
    $.each(Lists, (index, value)=> {
        $('#cardsshowall')

            .append(part1 + left_after_part1 + part2 + part3 + part4 + part5 + value.name + part52 + part6 + index + part66 + part7);
        left_after_part1 = left_after_part1 + 350;
        generealIndex = index;
        //$('#card' + index).attr("value", value.id);
        getCards(value.id, $('#card' + index));

    });


};

export function activeAllDataAfterAddCard() {
    getAllLists("nofilter");
}


let getAllLists = (activatedFrom)=> {

    $('#cardsshowall').empty();
    $('#filter2').show(1000);
    let boardId = document.getElementById('boards')[1].value;

    if (activatedFrom == "nofilter") {
        Trello.get(
            '/boards/' + boardId + '/lists/',
            loadedList,
            function () {
                console.log("Failed to load labels");
            }
        );
    }
    else if (activatedFrom == "members") {
        Trello.get(
            '/boards/' + boardId + '/members/',
            GetAllCards,
            function () {
                console.log("Failed");
            }
        );
    }
    else {
        Trello.get(
            '/boards/' + boardId + '/lists/',
            loadedListwithfilter,
            function () {
                console.log("Failed to load labels");
            }
        );
    }
};


$('#all').click(()=> {
    getAllLists("nofilter");
});

