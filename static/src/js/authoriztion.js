import {loadBoards} from './boardloader';
console.log(loadBoards);

function authorize() {
    console.log('got here');
    Trello.authorize({
        type: "popup",
        name: "Trello dashboard",
        scope: {
            read: true,
            write: true
        },
        expiration: "never",
        success: loadBoards,
        error: function () {
            console.log("Failed authentication");
        }
    });
}


export default authorize()