let ShowCards = (cardArea, listarea, Card,index)=> {

        $('#cardsshowall').hide(200);
        $('#filterdarea').show(1000);


        let finishColor;
        let lastDate;
        if (Card.closed == true) {
            finishColor = "green";
        }
        else {
            finishColor = "red";
        }
        if (Card.due != null) {
            let stringDate = String(Card.due);
            lastDate = stringDate.split('T')[0]
        }
        else {
            lastDate = "There is no end date"
        }

        cardArea.append('<div' + ' id=' + Card.id + ' ><p>' + Card.name + '</p></div>').css('color', finishColor);
        $('#' + Card.id).append(
                '<div class="modal-content" style="display: none" >' +
                '<div class="modal-header">' +
                '<span class="close">&times</span>' +
                '<h2>' + Card.name + '</h2>' + '</div>' + '<div class="modal-body">'
                + '<p>' + 'Status: ' + Card.closed + '</p>' +
                '<p>' + 'Last Date: ' + lastDate + '</p>' +
                '</div>' +
                '<div class="modal-footer">' + '</div>' + '</div>'
            )

            .hover(
                ()=> {
                    let childModal = $('#' + Card.id).children('div');
                    console.log(childModal);
                    childModal.show();
                }
            ).mouseout(()=> {
            let childModal = $('#' + Card.id).children('div');
            console.log(childModal);
            childModal.hide();
        });


        cardArea.height(function (index, height) {
            return (height + 155);
        });
        listarea.position({
            of: $(window)
        });


        $('#filterdarea').show(2000);
    }
    ;


var loadedListwithfilter = (List, Card, leftElemnetMove, index)=> {////
    console.log('index' +index);
    console.log('leftElemnetMove' +leftElemnetMove);
    console.log('List' +List);
    console.log('Card' +Card);

    $('#filterdarea').empty();
    $('#filterdarea').hide(1000);

    let part0 = '<div + id=filterdList';
    let part1 = ' class="cards"  onClick="top.focus()" style="position: absolute; left:';
    let left_after_part1 = leftElemnetMove;
    let part2 = 'px">';
    let part3 = '<div class="w3-card-4" style="width:25%;height: 25px">';
    let part4 = '<header class="w3-container w3-blue" style="height: 75px">';
    let part5 = '<h1 style="font-size: large; font-family:Harrington">';
    let part52 = '</h1>' + '</header>';
    let part6 = '<div class="w3-container" style="height: 89px; font-family:Harrington"' + 'id=filterdcard';
    let part66 = '>' + '</div>';
    let part7 = '<footer class="w3-container w3-blue" style="height: 20px"> </footer> </div>';


    $('#filterdarea')

        .append(part0 + index + part1 + left_after_part1 + part2 + part3 + part4 + part5 + List.name + part52 + part6 + index + part66 + part7);
    ShowCards($('#filterdcard' + index), $('#filterdList' + index), Card,index);


};


export function GetAllCards(members) {

    let boardId = document.getElementById('boards')[1].value;
    Trello.get(
        '/boards/' + boardId + '/cards/',
        (cards)=> {
            GetMembersByResponsibleMembers(members, cards)
        },
        ()=> {
            console.log("Failed");
        }
    );
}


let GetMembersByResponsibleMembers = (allMembers, allCards)=> {
    let MemberFullName = $('#textfilterbox').val();
    let ChoosenCardsList = [];
    let ChoosenCards;
    ChoosenCards = allCards.filter((cards)=> {
        let index;
        let membersIndex;
        let CardMembersOwnersIdList = cards.idMembers;
        for (index = 0; index < CardMembersOwnersIdList.length; index++) {
            for (membersIndex = 0; membersIndex < allMembers.length; membersIndex++) {

                if (allMembers[membersIndex].id == CardMembersOwnersIdList[index] && allMembers[membersIndex].fullName == MemberFullName) {
                    return cards;
                }

            }

        }


    });
    console.log(ChoosenCards);
    GetAllListByCards(ChoosenCards);
};

let GetAllListByCards = (FilterdCards)=> {
    let leftElemnetMove = 150;
    $.each(FilterdCards, (index, card)=> {

        Trello.get(
            '/cards/' + card.id + '/list/',
            (lists)=> {
                loadedListwithfilter(lists, card, leftElemnetMove, index)
            },
            ()=> {
                console.log("Failed");
            }
        );
        leftElemnetMove = leftElemnetMove + 350;

    });


};
