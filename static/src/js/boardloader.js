/**
 * Created by DEV on 6/3/2016.
 */
var loadedBoards = function(boards){
    $.each(boards, function(index, value){
        $('#boards')
            .append($("<option></option>")
                .attr("value", value.id)
                .text(value.name));
    });
};

export function loadBoards() {
    Trello.get(
        '/members/me/boards/',
        loadedBoards,
        function () {
            console.log("Failed to load boards");
        }
    );
}
